---
label: Введение в переменные
order: 0
---

В полях, отмеченных значком `{}`, можно использовать переменные. Например:

![](../../static/images/variables_01.png)

В этом примере мы использовали _встроенную_ переменную `client.name` — имя клиента. Некоторые переменные доступны только в определенном контексте.

[!ref](./internal_variables)

## Если переменная не заполнена { #variable-not-filled-empty }

Так бывает, что не всегда данные в карточке клиента заполнены. Для таких случаев желательно указывать значение по-умолчанию. Например:

```
Добрый день, @{name||'уважаемый клиент'}!
```

В данном случае, если имя клиента не задано, то на выходе мы получим следующее сообщение:

```
Добрый день, уважаемый клиент!
```

Это гораздо лучше, чем:

```
Добрый день, !
```

Старайтесь всегда указывать значение по-умолчанию, даже если вы уверены, что эти данные обязательно должны быть заполнены.

### Создание своих переменных { #creating-your-own-variables }

Вы можете создать свою переменную с помощью действия [Обновить переменные](../actions/auxiliary#update-variables), а затем использовать переменную как `@{$мояПеременная}`.

[!ref](./custom_variables)

### Выражения, операторы, математические функции { #expressions-operators-math-functions }

В нашей системе мы поддерживаем различные операторы, вызов функций. К примеру, сложение выглядит вот так: `@{ $i + 1 }`. Также можно форматировать дату, работать с коллекциями и строками.

[!ref](./syntax)

### Ограничения по работе с переменными { #work-restrictions }

Общий размер переменных в контексте не должен превышать 32КБ. Если вы пытаетесь обновить переменные и их размер после обновления превышает допустимый, то обновление не будет совершено.

||| :broken_heart: Как же быть?
Большие данные вы можете сохранять в [настройках проекта](../project_parameters) и использовать уже их.
||| Как увеличить лимит?
Обратитесь в тех. поддержку с аргументированным запросом, почему вам нужно увеличить лимит.
|||