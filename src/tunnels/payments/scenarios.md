---
label: Обработчик счетов
order: -30
---

![Форма создания и редактирования обработчика счетов](../../static/images/scenariors.png)

Обработчик счетов определяет вид счета, логику его работы, настройки платежей и другое.

## Базовые настройки { #basic-settings }

Укажите название обработчика счета, язык и валюту. Язык будет влиять на страницу счета, а валюта будет определять валюту всех платежей, которые будут созданы с помощью этого обработчика счета.

Давайте рассмотрим остальные поля во вкладе _Настройки_:

![](../../static/images/basic-settings.png)

Поле|Описание { class="compact" }
---------|--------
Платежная система|Выберите одну или несколько платежных систем. Валюта этих систем должна соответствовать валюте обработчика счета.
Время жизни счета|Если заполнить это поле, то счет будет автоматически отменяться по истечению периода.
Условия обслуживания|Загрузите PDF файл с условиями обслуживания. Этот файл будет представлен на странице счета пользователю.
Описание платежа для клиента|Укажите описание платежа. Это значение передается во внешнюю систему, и для некоторых из них является обязательным полем!
Сумма по умолчанию|Если сумма не указана в [действии](../actions/payments/#bill), либо в [активаторе](./bill_from_chat), будет использоваться эта сумма.
E-mail по умолчанию|Электронный адрес пользователя по умолчанию.
Телефон по умолчанию|Телефон пользователя по умолчанию.
Позиции по умолчанию|[Товарные позици](./invoice_items) и по умолчанию.

!!!
Обратите внимание, во многих полях можно использовать [переменные](../variables/start). Можно использовать параметры [активатора](./bill_from_chat), чтобы заполнять поля.
!!!

## Изменение внешнего вида { #change-in-appearance }

Во вкладке _Внешний вид_, вы можете изменить настройки страницы счета.

Поле|Описание { class="compact" }
---------|--------
Получатель платежа|Будет отображаться в графе _Получатель_. Можно не заполнять.
Логотип|Загрузите изображение, оно будет отображаться вверху как для [минилендинга](../../mlp/create).

## Автосчет { #automatic-account }

В этой вкладке вы можете изменить настройки [платежей из чата](../actions/payments#bill).

## Редирект { #redirect }

#### URL редиректа

После успешной оплаты, пользователь будет перенаправлен на указанный адрес (можно использовать переменные). Например, после оплаты курса, сразу перенаправляется на страницу самого курса.

## События по счету { #account-events }

В этом блоке вы можете указать действия по каждому [статусу платежа](./invoices). Работает это так: когда платеж получает определенный статус, выполняются действия из обработчика счетов, закрепленные за этим статусом. Например, когда выставляется счет, сразу выполняются действия из вкладки _Выставлено_.

Вы можете, к примеру, отправлять сообщение _Спасибо, мы получили оплату!_, когда платеж переходит в статус _Оплачен_. Понятное дело, что этим все не ограничивается, и вы можете выполнять любые действия, в том числе запускать другие сценарии для контроля оплаты (так называемый _добив_). Сценарий запускается на статусе _Выставлено_ и останавливается на статусе _Отменен_, _Оплата_, _Удален_.

Также в этих действия доступны [переменные для счетов](../variables/internal_variables#payments).