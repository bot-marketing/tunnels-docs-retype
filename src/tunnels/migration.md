---
label: Обзор изменений в новом интерфейсе
order: -10000
visibility: hidden
---

## Канва

![Вот так теперь выглядит сценарий](../static/images/kanva.png)

Теперь все шаги размещаются на канве и переходы по ним отображаются как стрелочки.

Также, мы берем за приоритет отображение всей важной информации прямо на канве, чтобы вы сразу понимали, что происходит: какие [переменные обновляются](./actions/auxiliary#update-variables), какие [HTTP-запросы](./actions/auxiliary#http-request) выполняются и т.д.

### Копирование шагов

Шаги, и не только их, можно копировать. Любой элемент или несколько элементов можно выбрать и скопировать.

Любые действия в сценарий можно отменять с помощью _Ctrl+Z_.

### Кнопки, которые встроены в интерфейс

![](../static/images/buttons2.png)

Теперь кнопки (они же быстрые ответы) и инлайн кнопки можно разместить прямо в шаге, в котором отправляется сообщение. И по этим кнопкам можно делать переходы на другие шаги.

!!!
Кнопки обрабатываются только, пока сценарий находится на этом шаге. Если вы добавляете инлайн кнопки, которые закрепляются за сообщением, то они могут не сработать, если сценарий уже ушел дальше. Можно использовать [глобальные ветки](./introduction#global-events-handling), если нужны дополнительные обработчики.
!!!

## Другие изменения

### Задержка в шагах

Раньше задержка была ДО выполнения шага, теперь задержка будет ПОСЛЕ выполнения шага: теперь это задержка перехода. То есть сценарий переходит на шаг, выполняет действия, а затем ждет указанное время и совершает переход на следующий шаг (или завершается, если следующий шаг не указан).

Если вы используете в задержке какую-то конкретную дату, и эта дата уже прошла, то шаг НЕ будет пропущен. Если дата в прошлом, то она считается нулевой и сценарий сразу переходит на следующий шаг.

### Приоритет событий

Раньше входящие обрабатывались по зафиксированному приоритету. Теперь они обрабатываются ровно в той последовательности, в которой добавлены на канву. Если вы добавите обработку “Любое сообщение” раньше более конкретных обработчиков, то “Любое сообщение” будет перехватывать все сообщения.

![](../static/images/buttons3.png)