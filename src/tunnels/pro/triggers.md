---
label: Триггеры
order: -10
---

Триггер — это набор [действий](../actions), который можно выполнить из другого действия, из макроса или по ссылке. В триггерах возможно использовать параметры; они передаются в ссылке или указываются в действии.

Триггеры используются в следующих случаях:

 - Группировка часто повторяющихся действий. Вы создаете триггер из нескольких действий и используете его в разных местах, чтобы не добавлять одни и те же действия.
 - Выполнение действий из внешнего сервиса. Например, отправка уведомления, когда происходит какое-то событие на внешнем сервисе.
 - Выполнение действий через макрос. Вы можете выполнять какие-то действия непосредственно из чата с помощью вызова макроса. Например, запустить какой-то сценарий, или выставить счет.

## Как работает триггер {#work-info}

Триггер выполняется в том контексте, в котором запускается. Например, если вы его запускаете из сценария, то триггер будет видеть все переменные сценария, а также сможет их обновлять. Представьте, что действия из триггера просто вставляются на место вызова триггера.

Когда вы вызываете триггер по ссылке, то он запускается в отдельном контексте. Созданные переменные в таком триггере исчезают после его выполнения.

## Создание и изменение триггера { #creating-and-modifying-a-trigger }

![](../../static/images/creating-and-modifying-a-trigger.png)

Так выглядит простейший триггер. При его создании необходимо указать название и действия. Также, вы можете задать параметры по умолчанию. После создания триггер будет выглядеть так:

![](../../static/images/creating-and-modifying-a-trigger2.png)

Здесь видны ссылки для запуска триггера из внешнего сервиса и через макрос. Примеры использования и настройки будут представлены ниже.

Для редактирования триггера нажмите на его название.

### Использование параметров { #using-parameters }

В триггерах вы можете использовать параметры в любом действии; их можно использовать как переменную в тексте, так и для условий. Для обращения к параметру нужно знать только его имя, вы сами ответственны за определение типа параметра и то, передается он или нет.

#### В действиях { #in-action }

Переменная `query` содержит все параметры. К ним можно обращаться по ключу. Например, `@{query.name}`: здесь мы отобразим значение параметра `name`. Если параметр содержит пробелы, то к нему можно обратиться вот так: `@{query['Параметр с пробелом']}`.

#### В условиях { #in-conditions }

Для добавления условия, мы выбираем переменную _Параметры_ и в поле _Индекс_ указываем название параметра, например:

![](../../static/images/in-conditions-trigger.png)

#### Параметры по умолчанию { #default-options }

При создании триггера вы можете указать список параметров и их значения по умолчанию. Эти значения будут использовать в том случае, если при запуске триггера соответствующие параметры не были указаны.

#### Пример использования параметра { #trigger_param_example }

![](../../static/images/trigger_param_example.png)

В данном случае мы используем параметр `имя`. По умолчанию, значение равняется `мир`, и, если мы не передаем этот параметр, нам будет отправлено сообщение `Привет, мир!`.

## Вызов триггера { #call-trigger }

### Вызов из действия { #call-from-action }

Для вызова триггера из действия (вы можете даже вызвать один триггер из другого), воспользуйтесь действием [Вызвать триггер](../actions/auxiliary#call-trigger):

![](../../static/images/call-from-action.png)

Выберите из списка нужный триггер и, если это необходимо, [укажите параметры](../project_parameters#key-value).

### По ссылке { #link }

Ссылку на триггер можно получить после его создания. Она отобразится в поле URL для запуска. Пример такой ссылки:

```
https://mssg.su/h/8g9UbRKV/{chatTriggerId}
```

В этой ссылке присутствует переменная `@{chatTriggerId}` — она необходима для запуска. Внешний сервис, с которого будет открываться ссылка, должен изначально получить этот параметр. Процесс обычно выглядит так:

 - Клиент заходит в мессенджер, мы делаем запрос во внешний сервис, в котором передаем `@{chatTriggerId}`, например через веб-хук: `http://myservice.com/register?chatTriggerId={chatTriggerId}`
 - Внешний сервис сохраняет это значение на своей стороне 
 - Если происходит событие на внешнем сервисе, он вызывает нужный триггер и подставляет туда полученный `chatTriggerId`.

Чтобы передать параметр, укажите его прямо в ссылке, например:

```
https://mssg.su/h/7XDRUB5O/{chatTriggerId}?name=Александр
```
