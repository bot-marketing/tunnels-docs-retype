---
label: Отправка сообщений
order: -10
---

## Отправить текст { #send-text }

![](../../static/images/send-text.png)

Данное действие используется для отправки сообщения клиенту в чате, либо для запроса у клиента какой-либо реакции.

Параметр|Raw|Тип|Ограничения|Описание { class="compact" }
--------|---|---|-----------|--------
Текст|`text`|строка|4096 символов|Текст сообщения. [Рандомизация текста](#message_randomizing)
Открыть диалог|`openDialog`|флаг|&mdash;|Если обращение закрыто, то мы откроем новое
Клавиатура|`keyboard`|объект|Нельзя указывать вместе с `buttons`|Только для raw
Кнопки клавиатуры|`keyboard.buttons`|массив объектов|&mdash;|Список кнопок
Тип кнопки|`keyboard.buttons.*.type`|`reply`,`phone`,`location`|&mdash;|По умолчанию `reply`
Текст кнопки|`keyboard.buttons.*.text`|строка|&mdash;|
Цвет кнопки|`keyboard.buttons.*.color`|`white`,`red`,`blue`,`green`|&mdash;|
Инлайн кнопки|`buttons`|массив объектов|Нельзя указывать вместе с `keyboard`|Только для raw
Тип инлайн кнопки|`buttons.*.type`|`reply`,`url`|&mdash;|По умолчанию `reply`
Текст инлайн кнопки|`buttons.*.text`|строка|&mdash;|
Ссылка инлайн кнопки|`buttons.*.url`|строка|только для типа `url`|
Данные инлайн кнопки|`buttons.*.payload`|строка|&mdash;|Обязательно для заполнения. Когда пользователь будет кликать по кнопке, будет приходить это значение в чат.


Статус|Описание { class="compact" }
------|--------
200|Сообщение отправлено успешно
400|Ошибка в параметрах действия
412|Не выполнены условия

[!ref Как внести переменную в текст](../variables/start)

## Отправить изображение { #send-image }

![](../../static/images/send-image.png)

С помощью этого действия можно отправить картинку с опциональной подписью.

Параметр|Raw|Тип|Ограничения|Описание { class="compact" }
--------|---|---|-----------|--------
Изображение|`url`|строка|не более 16МБ|Загрузка изображения 
Подпись|`caption`|строка|не более 4000 символов|Подпись под изображением. Опционально.

Статус|Описание { class="compact" }
------|--------
200|Изображение отправлено успешно
400|Ошибка в параметрах действия
412|Не выполнены условия

## Отправить видео { #send-video }

![](../../static/images/send-video.png)

Отправить видео пользователю.

Параметр|Raw|Тип| Ограничения|Описание { class="compact" }
--------|---|---|------------|--------
Файл|`url`|строка|Максимальный размер файла: 20МБ. <br>Поддерживаемые форматы: зависит от мессенджера, но mp4 поддерживается всеми|Загрузка видео
Подпись|`caption`|строка|Не более 4000 символов| Текст под видео. Опционально.

Статус|Описание { class="compact" }
------|--------
200|Видео отправлено успешно
400|Ошибка в параметрах действия
412|Не выполнены условия

## Отправить аудио { #send-audio }

![](../../static/images/send-audio.png)

Отправка аудио сообщения пользователю.

Параметр|Raw|Тип|Ограничения|Описание { class="compact" }
--------|---|---|-----------|--------
Файл|`url`|строка| Файл не более 20МБ |Для отправки голосовых сообщений загрузите файл ogg не более 1МБ и не более 5 минут. <br>Поддерживаются файл ogg, m4a, mp3
Подпись|`caption`|строка|Не более 4000 символов.<br>Поддерживается не всеми мессенджерами) |Подпись под файлом. Опционально.

Статус|Описание { class="compact" }
------|--------
200|Аудио сообщение отправлено успешно
400|Ошибка в параметрах действия
412|Не выполнены условия

## Отправить документ { #send-doc }

Параметр| Raw |Тип|Ограничения|Описание { class="compact" }
--------|--|---|-----------|--------
Файл |`url`|строка|Не более 20МБ| Отправить документ
Подпись |`caption`|строка|Не более 4000 символов.<br>Поддерживается не всеми мессенджерами|Подпись под файлом. Опционально.

Статус|Описание { class="compact" }
------|--------
200|Документ отправлен успешно
400|Ошибка в параметрах действия
412|Не выполнены условия

## Рандомизация текста {#message_randomizing}

Для лучшего пользовательского опыта мы рекомендуем рандомизировать ваши сообщения. Особенно это касается тех случаев, когда клиент может получить несколько одинаковых сообщений, каких-то уведомлений.

Есть два способа рандомизации: при отправке сообщения вы можете указать несколько вариантов написания одного и того же сообщения по смыслу, а также можете использовать рандомизацию фраз внутри сообщения.

Конструкция вида `[[A|B|C]]` в тексте при форматировании превращается либо в А, либо в В, либо в С.
Например. У нас есть фраза “Здравствуйте, как дела?”. Вот такой конструкцией мы можем разнообразить одну и ту же фразу по смыслу:

```
[[Привет|Приветствую|Здравствуйте]], [[как жизнь|все хорошо]]?
```

Теперь одно сообщение может отправиться шестью способами!

```
Привет, как жизнь?
Привет, все хорошо?
Приветствую, как жизнь?
Приветствую, все хорошо?
Здравствуйте, как жизнь?
Здравствуйте, все хорошо?
```