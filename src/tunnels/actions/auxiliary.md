---
label: Вспомогательные действия
order: -30
---

## Запустить сценарий { #tunnel-start }

Асинхронный — сценарий запускается параллельно. Синхронно — текущие действия прерывается и запускается указанный сценарий. Когда он завершает работу, то продолжают выполняться действия дальше, при этом передается переменная `result` из запущенного сценария. Получается, что сценарий работает как функция, которая что-то делает и возвращает результат.

[!ref](../pro/tunnel_as_a_function)

### Асинхронный запуск { #async-start }

![](../../static/images/tunnel_start_1.png?rand=1)

Параметр| Raw|Тип|Ограничения|Описание { class="compact" }
--------|----|---|-----------|--------
Сценарий| `tunnelId` &mdash; идентификатор сценария |Число|&mdash;|Сценарий, который нужно запустить
Тип запуска| `sync = false` |Флаг|&mdash;|Асинхронный
Скоуп| `scope` |Строка|&mdash;|Это текстовая строка, которая определяет группу сценариев. [Подробнее](../pro/scope).
Перезапустить сценарий| `force` |Флаг|&mdash;|Сценарий будет перезапущен, если флаг отмечен
Передать переменные| &mdash; |&mdash;|&mdash;|Укажите _названия_ переменных, которые будут переданы другому сценарию. Эти переменные будут просто переданы из текущего контекста.
Переменные| `variables` |Объект|&mdash;|Укажите название и значения переменные, которые будут переданы другому сценарию. То есть в запущенном сценарии будут доступны переменные с указанными названиями и значениями. Данный блок идентичен действию [Обновить переменные](#update-variables).

Статус|Описание { class="compact" }
------|--------
200|Сценарий запущен успешно
400|Ошибка в параметрах действия
412|Не выполнены условия

!!!
После выполнения этого действия будет доступна переменная `startedTunnelSessionHash`, которую можно использовать для выполнения [внешних запросов](../pro/external_requests.md) к сессии.
!!!

### Синхронный запуск

![](../../static/images/tunnel_start_2.png?rand=1)

Параметр|Raw|Тип|Ограничения|Описание { class="compact" }
--------|---|---|-----------|--------
Сценарий|`tunnelId` &mdash; идентификатор сценария|Число|&mdash;|Сценарий, который нужно запустить
Тип запуска|`sync = true`|Флаг|&mdash;|Синхронный
Переменная для сохранения|`syncVar`|Строка|&mdash;|В эту переменную будет возвращен результат из запускаемого сценария после его завершения. По умолчанию эта переменная — `result`.
Передать переменные|&mdash;|&mdash;|&mdash;|Укажите _названия_ переменных, которые будут переданы другому сценарию. Эти переменные будут просто переданы из текущего контекста.
Переменные|`variables`|Объект|&mdash;|Укажите название и значения переменные, которые будут переданы другому сценарию. То есть в запущенном сценарии00 будут доступны переменные с указанными названиями и значениями. Данный блок идентичен действию [Обновить переменные](#update-variables).

Статус|Описание { class="compact" }
------|--------
200|Сценарий запущен успешно
400|Ошибка в параметрах действия
412|Не выполнены условия

## Остановить сценарий { #tunnel-stop }

![](../../static/images/tunnel-stop.png)

Данное действие позволит вам остановить указанный сценарий.

Параметр| Raw |Тип|Ограничения|Описание { class="compact" }
--------|--|---|-----------|--------
Сценарий| `tunnelId` |число|&mdash|Сценарий, который нужно остановить.
Скоуп| `scope` |строка|&mdash|Это текстовая строка, которая определяет группу сценариев. [Подробнее](../pro/scope).

Статус|Описание { class="compact" }
------|--------
200|Действие выполнено успешно
400|Ошибка в параметрах действия
412|Не выполнены условия

## Обновить переменные { #update-variables }

![](../../static/images/update-variables.png)

С помощью данного действия можно создавать [свои переменные](../variables/custom_variables).

Параметр| Raw |Тип|Ограничения|Описание { class="compact" }
--------|--|---|------|--------
Переменные| variables | объект|&mdash;|Укажите ключ и значение переменной. Как ключ, так и значение,<br> могут содержать другие переменные.

Статус|Описание { class="compact" }
------|--------
200|Переменные успешно обновлены
304|Нет данных для обновления
412|Не выполнены условия
413|Превышен лимит на объем переменных

## HTTP-запрос { #http-request }

![Общий вид действия HTTP запрос](../../static/images/http-request.png)

Данное действие позволяет делать HTTP запросы на сторонние сервисы. Вы можете изменить тип запроса, адрес, тело запроса, заголовки, а также получить результат в одном из доступных форматов.

Параметр| Raw |Тип|Ограничения|Описание { class="compact" }
--------|--|---|------|--------
Тип запроса|&mdash;|&mdash;|GET,POST,PUT,PATCH,DELETE|Тип запроса
URL|&mdash;|&mdash;|&mdash;|Укажите URL адрес внешнего сервиса (можно использовать [переменные](../variables/variables_in_url)).
Тело запроса|&mdash;|&mdash;|&mdash;|Для всех типов запроса, кроме GET, возможно указать тело запроса в одном из форматов: `Параметры формы`, `JSON` и `Свободный формат`
Параметры формы|&mdash;|&mdash;|Для запроса `Параметры формы`|Можно передать параметры как ключ/значение. Устанавливается заголовок `Content-Type: application/x-www-form-urlencoded`.
JSON|&mdash;|&mdash;|Для запроса `JSON`|Тело запроса передается как JSON. Устанавливается заголовок `Content-Type: application/application/json`.
Свободный формат|&mdash;|&mdash;|Для запроса `Свободный формат`|Тело передается как есть. Если необходимо, укажите дополнительные заголовки.
Заголовки|&mdash;|&mdash;|&mdash;|Дополнительные заголовки к запросу
Формат ответа|&mdash;|&mdash;|&mdash;|Если вы ожидаете ответ от сервиса можно указать один из форматов: `json` или `string`
Обновить переменные|&mdash;|&mdash;|&mdash;|Укажите переменные, которые будут обновлены, когда будет получен ответ. Данные ответа будут доступны из встроенной переменной `httpResponse`, см. ниже

### Результат выполнения действия

После выполнения действия вы можете получить HTTP статус ответа из переменной `@{httpResponse.status}`, а тело ответа из переменной `@{httpResponse.body}`. Если вы указали тип ответа `json`, то в этой переменной будет полноценный объект. Вы сможете получить доступ к любым его полям, например `@{httpResponse.body.name}`. В поле `@{httpResponse.headers}` хранятся все заголовки ответа. Заголовки записаны в нижнем регистре, вот так можно достать заголовок: `@{httpResponse.headers['content-type']}`.

Если запрос не удалось выполнить, в переменной `@{httpResponse.error}` будет содержаться код ошибки.

Коды ошибок|Описание ошибки { class="compact" }
-----------|---------------
response_body_too_big|Тело ответа превышает лимит 128КБ
invalid_response_json|Тело ответа не является корректным JSON объектом
failed_to_connect|Не удалось подключиться к удаленному серверу
timed_out|Сервис не вернул ответ вовремя. В контексте логов будет отображаться максимальное время на обработку запроса.<br>По стандарту это 10 секунд.

Статус|Описание { class="compact" }
------|--------
200|Действие успешно выполнено
304|Запрос выполнен успешно, но никакие переменные не обновлены
400|Ошибка в параметрах действия
412|Не выполнены условия
413|Тело ответа превышает лимит, либо обновление переменных превышает лимит
500|Другие ошибки
502|Не удалось подключится к серверу
504|Таймаут запроса

## Вызвать триггер { #call-trigger }

![](../../static/images/call-trigger.png)

С помощью этого действия можно вызвать [триггер](../pro/triggers).

| Параметр  | Raw     | Тип                                                                                  | Ограничения | Описание { class="compact" } |
|-----------|---------|--------------------------------------------------------------------------------------|-------------|------------------------------|
| Триггер   | `id`    | число (идентификатор триггера) / строка (код триггера)                               | &mdash;     | Нужный триггер.              |
| Параметры | `query` | массив объектов с полями `name` &mdash; название параметра, `value` &mdash; значение | &mdash;     | Параметры триггера.          |

Статус|Описание { class="compact" }
------|--------
200|Действие выполнено успешно
400|Ошибка в параметрах действия
412|Не выполнены условия
413|Превышен лимит на размер параметров

## Системное сообщение { #system-message }

![](../../static/images/system-message.png)

С помощью данного действия вы можете отправить системное сообщение в чат. Такое сообщение видно только операторам и не отправляется пользователю.

Параметр| Raw |Тип|Ограничения|Описание { class="compact" }
--------|--|---|-----------|--------
Поле для сообщения|`text`|строка|Максимальная длина: 4000 символов.|Текст сообщения.

!!!
Отправка системных сообщений ограничена до 20 сообщений в минуту на аккаунт.
!!!

Статус|Описание { class="compact" }
------|--------
200|Действие выполнено успешно
400|Ошибка в параметрах действия
412|Не выполнены условия
429|Превышен лимит отправки системных сообщений

## E-mail уведомление { #email-notification }

Отправить уведомление на указанный почтовый адрес.

![](../../static/images/email-notification.png)

Параметр| Raw |Тип|Ограничения|Описание { class="compact" }
--------|--|---|-----------|--------
E-mail адрес получателя|`destination`|строка или массив строк|Адрес должен быть корректным e-mail|Один или несколько получателей, разделенных `,;|`, переводом строки или пробелом.
Тема письма|`subject`|строка|&mdash;|Тема письма.
Сообщение|`text`|строка|&mdash;|Текст сообщения.
Показать последние сообщения из чата|`withChat`|флаг|&mdash;|В тексте письма будут подгружены сообщения из чата.
Вложения|`attachments`|массив ссылок|&mdash;| Вложения к письму.

Статус|Описание { class="compact" }
------|--------
200|Действие выполнено успешно
400|Ошибка в параметрах действия
404|Почтовый сервер отклонил получателей
412|Не выполнены условия
500|Ошибка почтового сервера

## Вебхук { #webhook }

![](../../static/images/webhook.png)

С помощью этого действия можно отправлять данные во внешний сервис, при этом передаются данные текущего контекста. В ответ на вебхук можно возвращать действия.

[!ref Подробнее про вебхуки](../pro/webhook)

Параметр| Raw |Тип|Ограничения|Описание { class="compact" }
--------|--|---|-----------|--------
URL|url|строка|-|Адрес внешнего сервера.

Статус|Описание { class="compact" }
------|--------
200|Действие выполнено успешно
400|Ошибка в параметрах действия
412|Не выполнены условия
*|Любой код ответа внешнего сервера, если он не равен 200 или 202

[!ref](../variables/variables_in_url)
