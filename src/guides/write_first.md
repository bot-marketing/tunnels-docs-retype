# Написать клиенту по номеру телефона

Бывает такое, что вам нужно из какой-то внешней системы написать клиенту сообщения по номеру телефона.

Сделать это можно, но только в мессенджерах, которые позволяют писать первым: Whatsapp, Email и неофициальные подключения Telegram, Viber.

Делается это с помощью [триггеров](../tunnels/pro/triggers.md) и действия [Новому клиенту](../tunnels/actions/contacts_and_chats#new-client).

!!!
Прежде чем продолжить, рекомендуем ознакомиться с триггерами и с действием по ссылкам выше.
!!!

## Создание триггера

Создайте новый триггер и добавьте в него действие _Новому клиенту_. В поле номера телефона укажите `@{query.phone}` и добавьте нужные действия (например, [Отправить текст](../tunnels/actions/sending_messages#send-text)). Пример на скриншоте ниже:

![](../static/images/write_first_new_trigger.png)

После создания у триггера появляется ссылка на запуск, в нашем случае это `https://mssg.su/h/5OJUEo4V/{chatTriggerId}`. Но эта ссылка содержит переменную, так как триггер запускается всегда для какого-то чата.

## Получение ссылки для запроса

Создайте сценарий с одним шагом. Добавьте в него действие "Отправить текст" и вставьте в качестве текста ту самую ссылку на триггер, как показано на скриншоте ниже:

![](../static/images/write_first_new_scenario.png)

Выберите чат на том мессенджере, с которого вы хотите отправлять сообщения клиентам.

!!!
Убедитесь, что нужный мессенджер подключен к проекту.
!!!

Запустите сценарий по ссылке или по коду в нужном чате. Вам придет сообщение с полной ссылкой на триггер:

![](../static/images/write_first_message.png)

## Написать клиенту по номеру телефона

Теперь вы можете делать запросы по этой ссылке. Например, чтобы отправить сообщение на номер клиента, вы можете отправить запрос `GET https://mssg.su/h/5OJUEo4V/13985342/19996/whatsapp?phone=79205550123`.

Для теста просто откройте ссылку в браузере:

![](../static/images/write_first_browser.png)

Готово!

## Итоги

Мы создали триггер, который позволяет отправить сообщение клиенту по номеру телефона. Но этим не ограничивается! Вы можете запускать сценарии, отправлять файлы или выполнять любые другие действия.

Номер телефона передается как параметр триггера, а вы можете передавать любые другие параметры и использовать их в сценариях.

Запрос можно выполнить также и методом `POST`, а данные передать в формате `json` или `urlencoded`.

Триггер запускается в выбранном вами чате, а с помощью действия _Новому клиенту_ создает или находит чат по номеру телефона. Вы можете смотреть логи по этому чату, чтобы отслеживать выполненные запросы к триггеру или отправлять системные сообщения с пометками.

[!ref](../tunnels/pro/triggers.md)