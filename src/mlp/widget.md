---
label: Свои кнопки на сайте с захватом UTM
order: -30
---

!!!
Перед прочтением этой статьи сначала прочитайте [что такое минилендинг и как он работает](start)!
!!!

Для этого нужно создать [минилендинг](create) и скопировать его код. Далее, добавить на ваш сайт в конец тела (перед `</body>`) следующий скрипт, где `WIDGET_CODE` заменить на код вашего виджета:

```
<script>
  (function(w,d,e,s,i){w[i]=w[i]||function(){(w[i].a=w[i].a||[]).push(arguments)};k=d.createElement(e),a=d.getElementsByTagName(e)[0],k.async=1,k.src=s,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mssg.su/widget/main.js", "bmw");
  bmw('WIDGET_CODE', 'init', 'https://:domain/api/public/widgets');
</script>
```

## Обработка кликов

Разместите нужные кнопки (Telegram, VK, Viber, Facebook, WhatsApp). Это могут быть любые элементы, стилизованные на ваш вкус, но чаще всего -- это ссылки.

### Если кнопки являются ссылками {#link-attrs}

Если кнопки являются ссылками, то в `href` нужно указать ссылку на запуск бота (можно брать ссылки из [старта по ссылке](../tunnels/start_stop#deep-link) для сценария), но в тексте должна быть переменная `{code}` вместо реального кода сценария. Эту переменную мы заменим на специальный код при клике по кнопке. Пример таких кнопок:

```
<a href="https://t.me/marketing_support_bot?start={code}" data-bm-widget="UXaT9FmD" data-bm-messenger="telegram">Telegram</a>
<a href="https://wa.me/79066796730?text=%D0%A7%D1%82%D0%BE%D0%B1%D1%8B%20%D0%BD%D0%B0%D1%87%D0%B0%D1%82%D1%8C%20%D0%B4%D0%B8%D0%B0%D0%BB%D0%BE%D0%B3%2C%20%D0%BE%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D1%8C%D1%82%D0%B5%20%D1%8D%D1%82%D0%BE%20%D1%81%D0%BE%D0%BE%D0%B1%D1%89%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%BA%D0%B0%D0%BA%20%D0%B5%D1%81%D1%82%D1%8C%20{code}" data-bm-widget="UXaT9FmD" data-bm-messenger="whatsapp">WhatsApp</a>
```

Для ссылок заполните атрибут `data-bm-widget`, там должен быть указан код минилендинга. В атрибуте `data-bm-messenger` укажите тип мессенджера.
Теперь при клике по этим кнопкам, мы будем перенаправлять пользователя в мессенджер, а после перехода будут запущены действия из минилендинга.

### Ручная обработка клика {#manual}

Также можно обрабатывать клики вручную с помощью метода `bmw('WIDGET_CODE', 'click', 'LINK {code}', 'MESSENGER', variables, blank = false)`. Пример для телеграма:

```
<button type="button" onclick="bmw('UXaT9FmD', 'click', 'https://t.me/marketing_support_bot?start={code}', 'telegram');return false;">Telegram</button>
```

!!!
Если вы хотите получать UTM-метки и другие параметры, доступные только для виджетов, а также видеть адекватную статистику по минилендингу, то ОБЯЗАТЕЛЬНО использовать плейсхолдер {code} в ссылках!
!!!

## Редирект в мессенджер с захватом UTM {#redirect-with-utm}

Чтобы на странице сделать редирект сразу в whatsapp (или другой мессенджер), используйте скрипт ниже, заполните переменную `widgetCode` (код минилендинга), `whatsappPhone` — номер телефона whatsapp в формате `79205550123`. Разместите скрипт на странице, на которой нужно делать редирект.

```
<script>
  var widgetCode = 'WIDGET_CODE';
  var whatsappPhone = '79205550123';
  (function(w,d,e,s,i){w&#091;i]=w&#091;i]||function(){(w&#091;i].a=w&#091;i].a||&#091;]).push(arguments)};k=d.createElement(e),a=d.getElementsByTagName(e)&#091;0],k.async=1,k.src=s,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mssg.su/widget/main.js", "bmw");
  bmw(widgetCode, 'init', 'https://:domain/api/public/widgets', function(ok) { ok && bmw(widgetCode, 'click', 'https://wa.me/'+whatsappPhone+'?text=%D0%A7%D1%82%D0%BE%D0%B1%D1%8B%20%D0%BD%D0%B0%D1%87%D0%B0%D1%82%D1%8C%20%D0%B4%D0%B8%D0%B0%D0%BB%D0%BE%D0%B3%2C%20%D0%BE%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D1%8C%D1%82%D0%B5%20%D1%8D%D1%82%D0%BE%20%D1%81%D0%BE%D0%BE%D0%B1%D1%89%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%BA%D0%B0%D0%BA%20%D0%B5%D1%81%D1%82%D1%8C%20{code}', 'whatsapp'); });
</script>
```

## Передача переменных в виджет {#pass-variables}

С помощью параметра `variables` функции `bmw` можно передать свои данные и использовать их в виджете (минилендинге) как переменные, например:

```
<button type="button" onclick="bmw('UXaT9FmD', 'click', 'https://t.me/marketing_support_bot?start={code}', 'telegram', {id:123});return false;">Telegram</button>
```

В виджете будет доступна переменная `{$id}` со значением 123.

!!!
Параметр `variables` должен быть объектом!
!!!